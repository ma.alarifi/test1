package tests;

import org.junit.Test;
import Utilities.MatrixMethod;
import junit.framework.TestCase;
import static org.junit.Assert.assertArrayEquals;

public class MatrixMethodTests {

	//tests the duplicate method in the MatrixMethod class
	@Test
	public void testDuplicate(){
		int[][] a = {{1,10,34}, {27, 45, 11}, {32, -10, 0}};
		int[][] expected = {{1, 1, 10, 10, 34, 34}, {27, 27, 45, 45, 11, 11}, {32, 32, -10, -10, 0, 0}};
		assertArrayEquals(expected, MatrixMethod.duplicate(a));
	}

}
