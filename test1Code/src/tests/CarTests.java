package tests;

import org.junit.Test;

import automobiles.Car;
import junit.framework.TestCase;

public class CarTests extends TestCase {
	//ensures the constructor refuses negative values
	@Test
	public void testConstructor(){
		try{
			Car a = new Car(-10);
			
		fail("the constructor did not throw a constructor when it was meant to");
		}
		catch(IllegalArgumentException e){
			
		}
	}
	
	//ensuring get methods return the correct values
	@Test
	public void testGetMethods(){
		Car a = new Car(5);
		assertEquals(5, a.getSpeed());
		assertEquals(50, a.getLocation());
	}
	
	//ensuring move right moves the car right by its speed
	@Test
	public void testMoveRight(){
		Car a = new Car(5);
		a.moveRight();
		assertEquals(55, a.getLocation());
	}
	
	//ensuring move left moves the car left by its speed
	@Test
	public void testMoveLeft(){
		Car a = new Car(5);
		a.moveLeft();
		assertEquals(45, a.getLocation());
	}
	
	//ensures accelerate increments speed by one
	@Test
	public void testAccelerate(){
		Car a = new Car(5);
		a.accelerate();
		a.moveRight();
		assertEquals(56, a.getLocation());
	}
	
	//ensures stop sets speed to 0
	@Test
	public void testStop(){
		Car a = new Car(5);
		a.stop();
		assertEquals(0, a.getSpeed());
	}
}
