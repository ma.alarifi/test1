package Utilities;

public class MatrixMethod {
	//returns a 2d array which is the input 2d array but with an extra copy of each int value
	public static int[][] duplicate(int[][] original){
		int[][] clone = new int[original.length][];
		for(int i = 0; i < clone.length; i++){
			clone[i] = new int[original[i].length*2];
			for(int b = 0; b < clone[i].length; b += 2){
				clone[i][b] = original[i][b/2];
				clone[i][b+1] = original[i][b/2];
			}
		}
		return clone;
	}
}
